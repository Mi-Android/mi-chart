package inc.virak.vi_chart.ui.main;

public interface MainView
{
    void loadChart();
    void signUp();
}
