package inc.virak.vi_chart.data.firebase.model;

import java.util.Date;

public class Chat
{
    private User user;
    private String message;
    private Date date;
    public Chat(){}
    public Chat(User user, String message,Date date) {
        this.user = user;
        this.message = message;
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
