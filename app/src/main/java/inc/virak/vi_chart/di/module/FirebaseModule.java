package inc.virak.vi_chart.di.module;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseModule {

    @Provides
    @Singleton
    @Named("chats")
    public DatabaseReference provideChatDb()
    {
        return provideDb().child("chats");
    }
    @Provides
    @Singleton
    @Named("users")
    public DatabaseReference provideUserDb()
    {
        return provideDb().child("users");
    }
    @Provides
    @Singleton
    @Named("db")
    public DatabaseReference provideDb()
    {
        return FirebaseDatabase.getInstance().getReference();
    }
    @Provides
    @Singleton
    public FirebaseAuth provideAuth()
    {
        return FirebaseAuth.getInstance();
    }

}
