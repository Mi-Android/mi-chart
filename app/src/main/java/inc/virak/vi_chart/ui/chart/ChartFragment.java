package inc.virak.vi_chart.ui.chart;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Date;

import javax.inject.Inject;

import inc.virak.vi_chart.ChatApp;
import inc.virak.vi_chart.R;
import inc.virak.vi_chart.callback.DataListenter;
import inc.virak.vi_chart.ui.chart.adapter.ChatAdapter;
import inc.virak.vi_chart.data.firebase.model.Chat;
import inc.virak.vi_chart.data.firebase.model.User;
import inc.virak.vi_chart.utils.UserUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChartFragment extends Fragment implements ChartView{

    private ImageButton btnSend;
    private EditText edtMessage;
    private TextView tvTotalUser;
    private RecyclerView recyclerView;
    @Inject ChatPresenter<ChartView> presenter;
    @Inject ChatAdapter adapter;
    public ChartFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((ChatApp)getActivity().getApplication()).getAppComponent().inject(this);
        presenter.attatch(this);
        return inflater.inflate(R.layout.fragment_chart, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnSend = view.findViewById(R.id.btn_send);
        edtMessage = view.findViewById(R.id.edt_message);
        tvTotalUser = view.findViewById(R.id.tv_member_number);
        recyclerView = view.findViewById(R.id.recycler_view);

        presenter.addUserDataListenter(new DataListenter<Long>() {
            @Override
            public void onDataChanged(Long parameter) {
                refreshMember(parameter);
            }
        });

        adapter.attatch(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter.getAdapter());
        adapter.start();

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendChat(new Chat(new User(UserUtils.getMyId(),UserUtils.getEmail(),UserUtils.getUserName()),edtMessage.getText().toString(),new Date()));
                edtMessage.setText("");
                recyclerView.smoothScrollToPosition(adapter.getAdapter().getItemCount());
            }
        });
    }

    @Override
    public void onDataChange() {
        recyclerView.smoothScrollToPosition(adapter.getAdapter().getItemCount());
    }

    @Override
    public void refreshMember(long count) {
        tvTotalUser.setText(count+"");
    }

    @Override
    public Context geContext() {
        return this.getContext();
    }
}
