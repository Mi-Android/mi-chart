package inc.virak.vi_chart.ui.base;

public interface MvpPresenter<V> {
    void attatch(V view);
    void distatch();
    V getView();
}
