package inc.virak.vi_chart.ui.chart.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import inc.virak.vi_chart.R;
import inc.virak.vi_chart.data.firebase.model.Chat;
import inc.virak.vi_chart.utils.DateUtils;
import inc.virak.vi_chart.utils.UserUtils;

public class ChatViewHolder extends RecyclerView.ViewHolder {
    private TextView tvMessage;
    private TextView tvUserName;
    private LinearLayout layout;
    private TextView tvDate;
    RelativeLayout.LayoutParams params;
    public  ChatViewHolder(View view)
    {
        super(view);
        layout = view.findViewById(R.id.layout_message);
        params =(RelativeLayout.LayoutParams)layout.getLayoutParams();
        tvMessage = view.findViewById(R.id.tv_message);
        tvUserName = view.findViewById(R.id.tv_user);
        tvDate = view.findViewById(R.id.tv_date);

    }
    public void bindView(Chat chat)
    {
        tvMessage.setText(chat.getMessage());
        tvDate.setText(DateUtils.getDateFormat(chat.getDate()));
        if(UserUtils.isMe(chat.getUser()))
        {
            layout.removeView(tvUserName);
            myMessageLayout(layout);
        }
        else {

            tvUserName.setText(chat.getUser().getName());
            theirMessageLayout(layout);
        }
    }
    private void myMessageLayout(LinearLayout layout)
    {
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        params.setMargins(60,5,5,5);
        layout.setLayoutParams(params);
        layout.setBackgroundResource(R.drawable.my_message);
    }
    private void theirMessageLayout(LinearLayout layout)
    {
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.setMargins(5,5,100,5);
        layout.setLayoutParams(params);
        layout.setBackgroundResource(R.drawable.other_message);
    }
}
