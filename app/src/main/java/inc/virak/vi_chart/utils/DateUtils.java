package inc.virak.vi_chart.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils
{
    public static String getDateFormat(Date date)
    {
        SimpleDateFormat format = new SimpleDateFormat("dd-MMM-YY hh:mm");
        return format.format(date);
    }
}
