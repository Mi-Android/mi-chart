package inc.virak.vi_chart.ui.chart;

import android.content.Context;

public interface ChartView
{
    void onDataChange();
    void refreshMember(long count);
    Context geContext();

}
