package inc.virak.vi_chart.callback;

public interface DataListenter<T>
{
    void onDataChanged(T parameter);
}
