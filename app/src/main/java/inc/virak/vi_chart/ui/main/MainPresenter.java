package inc.virak.vi_chart.ui.main;

import javax.inject.Inject;

import inc.virak.vi_chart.data.firebase.model.User;
import inc.virak.vi_chart.data.firebase.repository.UserRepository;
import inc.virak.vi_chart.ui.base.BasePresenter;

public class MainPresenter<V> extends BasePresenter<V> {
    private MainView view;
    private UserRepository userRepository;
    @Inject
    public MainPresenter(UserRepository userRepository)
    {
        this.userRepository = userRepository;
    }
    public void checkUser()
    {
        if(userRepository.getCurrentUser() != null)
        {
            view.loadChart();
        }
        else
        {
            view.signUp();
        }
    }

    @Override
    public void attatch(V view) {
        super.attatch(view);
        this.view = (MainView) view;
    }
    public void addUser(User user)
    {
        userRepository.addUser(user);
    }

}
