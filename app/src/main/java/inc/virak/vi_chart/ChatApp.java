package inc.virak.vi_chart;

import android.app.Application;

import inc.virak.vi_chart.di.component.AppComponent;
import inc.virak.vi_chart.di.component.DaggerAppComponent;

public class ChatApp extends Application
{
    private AppComponent appComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
