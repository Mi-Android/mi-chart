package inc.virak.vi_chart.di.component;

import javax.inject.Singleton;

import dagger.Component;
import inc.virak.vi_chart.di.module.FirebaseModule;
import inc.virak.vi_chart.ui.chart.ChartFragment;
import inc.virak.vi_chart.ui.main.MainActivity;
@Singleton
@Component(modules = {FirebaseModule.class})
public interface AppComponent
{
    void inject(MainActivity activity);
    void inject(ChartFragment fragment);
}
